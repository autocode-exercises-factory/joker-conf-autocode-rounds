# План выступления

- Введение. Autocode - как и зачем, особые требования к тестам.
- Раунды - проходят в формате пинг-понга.
  - Раунд 1. Факториал и StackWalker API
    Нужно использовать рекурсию. StackWalker API
  - Раунд 2. 
    Нельзя использовать циклы и условные выражения. Java Parser 
  - Раунд 3.
    Нельзя использовать лямбды, метод референсы и стримы. Java Parser + JavaSymbolSolver. Spoon
  - Раунд 4. Проверяем, что мавен проект правильно строит артефакт. 
    MavenInvoker + Spoon. Программно делаем билд мавен проекта, анализируем джарник, запускаем его и тестируем.
- Заключение.
  Автотаски - это хорошо, а писать их интересно, вон какие необычные API мы сегодня посмотрели. А ведь можно и в продакшне добавлять в мавен проект плагин и запускать процессоры,
  которые будут обеспечивать статический анализ по локальным правилам (хапрет на методы с 5+ параметрами, например).

## Раунд 1. Факториал и StackWalker API

Нужно заставить написать рекурсивное решение.

```java
class Factorial {
  int factorial(int n) {
    return n < 2 ? 1 : n * factorial(n - 1);
  }
}
```

Должно быть рекурсивное решение. Добавляем к заданию специальный обсервер, который нужно вызывать каждый раз, когда
вычислено новое число. Условно правильное решение:

```java
class Factorial {

  public Factorial(final FactorialObserver observer) {
    this.observer = observer;
  }

  final FactorialObserver observer;

  int factorial(int n) {
    int result = n < 2 ? 1 : n * factorial(n - 1);
    observer.log(result);
    return result;
  }
}
```
Реализация обсервера:
```java
class FactorialObserverImpl implements FactorialObserver {

  List<Integer> log = new ArrayList<>();

  @Override
  public void log(final int counted) {
    log.add(counted);
  }

  @Override
  public long[] dumpLog() {
    final long[] result = log.stream().mapToLong(Integer::intValue).toArray();
    log = new ArrayList<>();
    return result;
  }
}
```

Тест:
```java
class FactorialTest {
    @Test
    void testFactorial() {
        final FactorialObserverImpl observer = new FactorialObserverImpl();
        final Factorial recursiveSum = new Factorial(observer);
        assertEquals(5040, recursiveSum.factorial(7));
        assertEquals("[1, 2, 6, 24, 120, 720, 5040]",
                Arrays.toString(observer.dumpLog()));
    }
}
```
Разумеется, можно решить задачу нерекурсивно, но тогда используем StackWalker API, чтобы это определить.
Затем развиваем тему и говорим, что нерекурсивное решение лушче, а еще лучше - использовать кэш.
И это тоже можно проверить с помощью StackWalker API.

## Раунд 2. Без циклов и условий. JavaParser
Нельзя использовать циклы и условия - (придумать причину)
Через небольшие тернии самостоятельного анализа кода берем JavaParser.
```java
class NoCyclesAndConditionsTest {

    @Test
    void testNoCyclesAndConditions() throws IOException {

        SourceRoot sourceRoot = new SourceRoot(Paths.get("src/main/java"));
        sourceRoot.getParserConfiguration().setLanguageLevel(ParserConfiguration.LanguageLevel.JAVA_11);

        final List<CompilationUnit> codeBase = sourceRoot.tryToParse().stream()
                .map(ParseResult::getResult)
                .flatMap(Optional::stream)
                .collect(Collectors.toList());

        codeBase.forEach(compilationUnit -> compilationUnit.accept(
                new VoidVisitorAdapter<Node>() {
                    @Override
                    public void visit(final IfStmt n, final Node arg) {
                        super.visit(n, arg);
                        fail("You must not use conditional statements");
                    }

                    @Override
                    public void visit(final ConditionalExpr n, final Node arg) {
                        super.visit(n, arg);
                        fail("You must not use conditional statements");
                    }

                    @Override
                    public void visit(final ForStmt n, final Node arg) {
                        super.visit(n, arg);
                        fail("You must not cycles");
                    }

                    @Override
                    public void visit(final ForEachStmt n, final Node arg) {
                        super.visit(n, arg);
                        fail("You must not cycles");
                    }

                    @Override
                    public void visit(final WhileStmt n, final Node arg) {
                        super.visit(n, arg);
                        fail("You must not cycles");
                    }

                    @Override
                    public void visit(final DoStmt n, final Node arg) {
                        super.visit(n, arg);
                        fail("You must not cycles");
                    }
                }, null));
    }
}
```

## Раунд 3. Без лямбд, метод-референсов и стримов. JavaParser + JSS -> Spoon
Начало - как в предыдущем раунде, но скоро понимаем, одного парсера мало, чтобы понять, используются ли стримы.
Для этого нужен еще и лексер - JavaSymbolSolver.
 
```java
class NoStreamsAndLambdasTest {

  @Test
  void testNoCycles() throws IOException {

    final JavaParserTypeSolver srcTypeSolver = new JavaParserTypeSolver(Paths.get("src/main/java"));
    final ReflectionTypeSolver jreTypeSolver = new ReflectionTypeSolver();
    TypeSolver typeSolver = new CombinedTypeSolver(jreTypeSolver, srcTypeSolver);

    JavaSymbolSolver symbolSolver = new JavaSymbolSolver(typeSolver);

    SourceRoot sourceRoot = new SourceRoot(Paths.get("src/main/java"));
    sourceRoot.getParserConfiguration().setSymbolResolver(symbolSolver);
    sourceRoot.getParserConfiguration().setLanguageLevel(ParserConfiguration.LanguageLevel.JAVA_11);
    sourceRoot.getParserConfiguration().setStoreTokens(true);

    final List<ParseResult<CompilationUnit>> parseResults = sourceRoot.tryToParse();
    final List<CompilationUnit> codeBase = sourceRoot.tryToParse().stream()
            .map(ParseResult::getResult)
            .flatMap(Optional::stream)
            .collect(Collectors.toList());

    codeBase.forEach(compilationUnit -> compilationUnit.accept(
            new VoidVisitorAdapter<Node>() {
              @Override
              public void visit(final LambdaExpr n, final Node arg) {
                super.visit(n, arg);
                fail("You must not use lambdas");
              }

              @Override
              public void visit(final MethodReferenceExpr n, final Node arg) {
                super.visit(n, arg);
                fail("You must not use method references");
              }

              @Override
              public void visit(final MethodCallExpr n, final Node arg) {
                super.visit(n, arg);
                final ResolvedType returnType = n.resolve().getReturnType();
              }
            }, null));

  }
}
```

Пробуем, но есть проблема солвинга, и мы переходим на Spoon.

```java
class CodeComplianceTest {

    private static CtPackage spoonRootPackage;
    private static CtTypeReference baseStreamRefType;

    @BeforeAll
    static void init() {
        final SpoonAPI spoon = new Launcher();
        spoon.addInputResource("src/main/java/");
        spoon.buildModel();
        spoonRootPackage = spoon.getFactory().Package().getRootPackage();
    }

    @Test
    void testNoStreams() {

        baseStreamRefType = new Launcher().getFactory().Code().createCtTypeReference(BaseStream.class);

        final List<CtInvocation> methodCallsReturningStreams = spoonRootPackage.getElements(
                new AbstractFilter<>() {
                    @Override
                    public boolean matches(final CtInvocation invocation) {
                        return invocation.getType().isSubtypeOf(baseStreamRefType);
                    }
                }
        );
        assertEquals(0, methodCallsReturningStreams.size(),
                () -> "You must not use streams in this exercises, " +
                        "but you have supplied " + methodCallsReturningStreams.size() + " of them: "
                        + methodCallsReturningStreams);
    }

    @Test
    void testNoLambdas() {
        final List<CtLambda> lambdas = spoonRootPackage.getElements(new TypeFilter<>(CtLambda.class));
        assertEquals(0, lambdas.size(),
                () -> "You must not use lambdas in this exercises, " +
                        "but you have supplied " + lambdas.size() + " of them: "
                        + lambdas);
    }

    @Test
    void testNoMethodRefs() {
        final List<CtExecutableReferenceExpression> methodRefs =
                spoonRootPackage.getElements(new TypeFilter<>(CtExecutableReferenceExpression.class));
        assertEquals(0, methodRefs.size(),
                () -> "You must not use method references in this exercises, " +
                        "but you have supplied " + methodRefs.size() + " of them: "
                        + methodRefs);
    }

}

```

## Раунд 4. Maven проект
TBD