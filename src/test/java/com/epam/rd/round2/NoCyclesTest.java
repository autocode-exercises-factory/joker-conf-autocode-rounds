package com.epam.rd.round2;

import com.github.javaparser.ParseResult;
import com.github.javaparser.ParserConfiguration;
import com.github.javaparser.ast.CompilationUnit;
import com.github.javaparser.ast.Node;
import com.github.javaparser.ast.expr.ConditionalExpr;
import com.github.javaparser.ast.stmt.DoStmt;
import com.github.javaparser.ast.stmt.ForEachStmt;
import com.github.javaparser.ast.stmt.ForStmt;
import com.github.javaparser.ast.stmt.IfStmt;
import com.github.javaparser.ast.stmt.WhileStmt;
import com.github.javaparser.ast.visitor.VoidVisitorAdapter;
import com.github.javaparser.utils.SourceRoot;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.nio.file.Paths;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.fail;

class NoCyclesAndConditionsTest {

    @Test
    void testNoCyclesAndConditions() throws IOException {

        SourceRoot sourceRoot = new SourceRoot(Paths.get("src/main/java"));
        sourceRoot.getParserConfiguration().setLanguageLevel(ParserConfiguration.LanguageLevel.JAVA_11);

        final List<CompilationUnit> codeBase = sourceRoot.tryToParse().stream()
                .map(ParseResult::getResult)
                .flatMap(Optional::stream)
                .collect(Collectors.toList());

        codeBase.forEach(compilationUnit -> compilationUnit.accept(
                new VoidVisitorAdapter<Node>() {
                    @Override
                    public void visit(final IfStmt n, final Node arg) {
                        super.visit(n, arg);
                        fail("You must not use conditional statements");
                    }

                    @Override
                    public void visit(final ConditionalExpr n, final Node arg) {
                        super.visit(n, arg);
                        fail("You must not use conditional statements");
                    }

                    @Override
                    public void visit(final ForStmt n, final Node arg) {
                        super.visit(n, arg);
                        fail("You must not cycles");
                    }

                    @Override
                    public void visit(final ForEachStmt n, final Node arg) {
                        super.visit(n, arg);
                        fail("You must not cycles");
                    }

                    @Override
                    public void visit(final WhileStmt n, final Node arg) {
                        super.visit(n, arg);
                        fail("You must not cycles");
                    }

                    @Override
                    public void visit(final DoStmt n, final Node arg) {
                        super.visit(n, arg);
                        fail("You must not cycles");
                    }
                }, null));
    }
}
