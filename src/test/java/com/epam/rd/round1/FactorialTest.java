package com.epam.rd.round1;

import org.junit.jupiter.api.Test;

import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.assertEquals;

class FactorialTest {

    @Test
    void testFactorial() {
        final FactorialObserverImpl observer = new FactorialObserverImpl();
        final Factorial recursiveSum = new Factorial(observer);
        assertEquals(5040, recursiveSum.factorial(7));
        assertEquals("[1, 2, 6, 24, 120, 720, 5040]",
                Arrays.toString(observer.dumpLog()));
    }

}