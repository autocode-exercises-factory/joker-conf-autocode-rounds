package com.epam.rd.round1;

import com.epam.rd.round1.FactorialObserver;

import java.util.ArrayList;
import java.util.List;

class FactorialObserverImpl implements FactorialObserver {

    List<Integer> log = new ArrayList<>();

    @Override
    public void log(final int counted) {
        log.add(counted);
    }

    @Override
    public long[] dumpLog() {
        final long[] result = log.stream().mapToLong(Integer::intValue).toArray();
        log = new ArrayList<>();
        return result;
    }
}
