package com.epam.rd.round3;

import com.github.javaparser.ParseResult;
import com.github.javaparser.ParserConfiguration;
import com.github.javaparser.ast.CompilationUnit;
import com.github.javaparser.ast.Node;
import com.github.javaparser.ast.expr.LambdaExpr;
import com.github.javaparser.ast.expr.MethodCallExpr;
import com.github.javaparser.ast.expr.MethodReferenceExpr;
import com.github.javaparser.ast.visitor.VoidVisitorAdapter;
import com.github.javaparser.resolution.types.ResolvedType;
import com.github.javaparser.symbolsolver.JavaSymbolSolver;
import com.github.javaparser.symbolsolver.model.resolution.TypeSolver;
import com.github.javaparser.symbolsolver.resolution.typesolvers.CombinedTypeSolver;
import com.github.javaparser.symbolsolver.resolution.typesolvers.JavaParserTypeSolver;
import com.github.javaparser.symbolsolver.resolution.typesolvers.ReflectionTypeSolver;
import com.github.javaparser.utils.SourceRoot;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.nio.file.Paths;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.fail;

class NoStreamsAndLambdasTest {

    @Test
    void testNoCycles() throws IOException {

        final JavaParserTypeSolver srcTypeSolver = new JavaParserTypeSolver(Paths.get("src/main/java"));
        final ReflectionTypeSolver jreTypeSolver = new ReflectionTypeSolver();
        TypeSolver typeSolver = new CombinedTypeSolver(jreTypeSolver, srcTypeSolver);

        JavaSymbolSolver symbolSolver = new JavaSymbolSolver(typeSolver);

        SourceRoot sourceRoot = new SourceRoot(Paths.get("src/main/java"));
        sourceRoot.getParserConfiguration().setSymbolResolver(symbolSolver);
        sourceRoot.getParserConfiguration().setLanguageLevel(ParserConfiguration.LanguageLevel.JAVA_11);
        sourceRoot.getParserConfiguration().setStoreTokens(true);

        final List<ParseResult<CompilationUnit>> parseResults = sourceRoot.tryToParse();
        final List<CompilationUnit> codeBase = sourceRoot.tryToParse().stream()
                .map(ParseResult::getResult)
                .flatMap(Optional::stream)
                .collect(Collectors.toList());

        codeBase.forEach(compilationUnit -> compilationUnit.accept(
                new VoidVisitorAdapter<Node>() {
                    @Override
                    public void visit(final LambdaExpr n, final Node arg) {
                        super.visit(n, arg);
                        fail("You must not use lambdas");
                    }

                    @Override
                    public void visit(final MethodReferenceExpr n, final Node arg) {
                        super.visit(n, arg);
                        fail("You must not use method references");
                    }

                    @Override
                    public void visit(final MethodCallExpr n, final Node arg) {
                        super.visit(n, arg);
                        final ResolvedType returnType = n.resolve().getReturnType();
                    }
                }, null));

    }
}