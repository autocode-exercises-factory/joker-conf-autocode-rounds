package com.epam.rd.round1;

class Factorial {

    public Factorial(final FactorialObserver observer) {
        this.observer = observer;
    }

    final FactorialObserver observer;

    int factorial(int n) {
        int result = n < 2 ? 1 : n * factorial(n - 1);
        observer.log(result);
        return result;
    }
}
