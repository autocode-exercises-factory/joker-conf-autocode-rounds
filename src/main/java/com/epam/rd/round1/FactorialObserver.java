package com.epam.rd.round1;

public interface FactorialObserver {
    void log(int counted);
    long[] dumpLog();
}
